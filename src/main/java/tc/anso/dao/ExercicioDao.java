package tc.anso.dao;

import java.sql.SQLException;

import tc.anso.entity.Exercicio;
import totalcross.sql.Connection;
import totalcross.sql.Statement;

public class ExercicioDao {

	private Connection con;
	private ConexaoUtil conexaoUtil;
	
	public ExercicioDao() {
		conexaoUtil = new ConexaoUtil();
		con = conexaoUtil.getConnection();
	}
	
	public void inserir(Exercicio exercicio) throws SQLException {
		Statement stmt = con.createStatement();
		stmt.executeUpdate("insert into exercicio "
				+ "values('"+exercicio.getObjetivo()+"','"+exercicio.getEquipamento()+"',"
						+ "'"+exercicio.getSeries()+"','"+exercicio.getNumRepeticoes()+"')");
		stmt.close();
	}
	
	public void executarQuery(String query) throws SQLException {
		Statement st2 = con.createStatement();
		st2.execute(query);
		st2.close();
	}
	
}
