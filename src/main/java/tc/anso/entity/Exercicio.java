package tc.anso.entity;

public class Exercicio {

	private String objetivo;
	private String equipamento;
	private String series;
	private String numRepeticoes;
	
	public String getObjetivo() {
		return objetivo;
	}
	public String getEquipamento() {
		return equipamento;
	}
	public String getSeries() {
		return series;
	}
	public String getNumRepeticoes() {
		return numRepeticoes;
	}
	public void setObjetivo(String objetivo) {
		this.objetivo = objetivo;
	}
	public void setEquipamento(String equipamento) {
		this.equipamento = equipamento;
	}
	public void setSeries(String series) {
		this.series = series;
	}
	public void setNumRepeticoes(String numRepeticoes) {
		this.numRepeticoes = numRepeticoes;
	}
	
}
