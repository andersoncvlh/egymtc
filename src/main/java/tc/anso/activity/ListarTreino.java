package tc.anso.activity;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import tc.anso.dao.ConexaoUtil;
import tc.anso.entity.Exercicio;
import totalcross.sql.Connection;
import totalcross.sql.DriverManager;
import totalcross.sql.ResultSet;
import totalcross.sql.Statement;
import totalcross.sys.Convert;
import totalcross.sys.Settings;
import totalcross.ui.Bar;
import totalcross.ui.Container;
import totalcross.ui.Grid;
import totalcross.ui.ScrollContainer;
import totalcross.ui.Spacer;
import totalcross.ui.Toast;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.font.Font;
import totalcross.ui.gfx.Color;
import totalcross.ui.gfx.Rect;
import totalcross.ui.image.Image;

public class ListarTreino extends Container {
	
	private ScrollContainer sc;
	private ConexaoUtil conexaoUtil;
	private Connection con;
	private List<Exercicio> listaExercicios;
	public ListarTreino() {
		
	}
	
	@Override
	public void initUI() {
		super.initUI();
		try {
			Bar bar = new Bar(" Listagem de Exerc�cios");
			bar.setForeColor(Color.getRGB(236, 240, 241));
			Image btn = new Image("/img/ic_action_sort_by_size.png");
			bar.addButton(btn);
			bar.setBackColor(Color.getRGB(46, 204, 113));
//			setMenuBar(bar);
//			add(menubar, CENTER,TOP);
			add(bar,LEFT,0,FILL,PREFERRED);
			listaExercicios= new ArrayList<Exercicio>();
			conexaoUtil = new ConexaoUtil();
			con = conexaoUtil.getConnection();
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("select * from exercicio");
			while (rs.next()) {
				Exercicio exercicio = new Exercicio();
				exercicio.setObjetivo(rs.getString("objetivo"));
				exercicio.setEquipamento(rs.getString("equipamento"));
				exercicio.setSeries(rs.getString("series"));
				exercicio.setNumRepeticoes(rs.getString("repeticoes"));
				listaExercicios.add(exercicio);
			}
			listaExercicios.size();
			st.close();
			
		} catch (Exception e) {
			Toast.show("Erro inesperado, contato o administrador.", 4000);
		}
		Rect r = getClientRect();
		String[]gridCaptions = {" Obetivo "," Equipamento "," S�ries "," Repeti��es "};
		int gridWidths[] = {-20, fm.stringWidth(" 000 "),-20,-10,};
		int gridAligns[] = { LEFT, LEFT, CENTER, CENTER, };
		Grid grid = new Grid(gridCaptions, gridWidths, gridAligns, false);
		Spacer sp = new Spacer(0,0);

		add(sp, CENTER,BOTTOM-200, PARENTSIZE+10, PREFERRED);
		add(grid, LEFT,TOP+220,PARENTSIZE,PARENTSIZEMIN,sp); 
		String[][] data = new String [listaExercicios.size()][4];
		
		if (!listaExercicios.isEmpty()) {
			for(int linha=0 ; linha<listaExercicios.size() ; linha++){
				for(int coluna = 0; coluna < 4 ; coluna ++){
					Exercicio item = listaExercicios.get(linha);
					String[] temp = {item.getObjetivo(),item.getEquipamento(),item.getSeries(),item.getNumRepeticoes()};
					data[linha][coluna]=temp[coluna];
				}
			}
		} else {
			Toast.foreColor=Color.WHITE;
			Toast.backColor=Color.getRGB(243, 156, 18);
			Toast.show("Lista vazia", 4000);
		}
		
		grid.setItems(data);
		grid.captionsBackColor = Color.getRGB(149, 165, 166);
		grid.firstStripeColor = Color.getRGB(236, 240, 241);
		grid.secondStripeColor = Color.getRGB(189, 195, 199);
	}
	
	@Override
	public void onEvent(Event event) {
		switch (event.type) {
		case ControlEvent.FOCUS_IN:
			System.out.println("FOCUS_IN");
			System.out.println(event.target);
			break;
		default:
			break;
	    }
	}
}
