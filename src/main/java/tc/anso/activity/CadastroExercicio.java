package tc.anso.activity;

import java.sql.SQLException;

import tc.anso.dao.ExercicioDao;
import tc.anso.entity.Exercicio;
import totalcross.io.IOException;
import totalcross.sys.Settings;
import totalcross.ui.Bar;
import totalcross.ui.Button;
import totalcross.ui.Check;
import totalcross.ui.ComboBox;
import totalcross.ui.Container;
import totalcross.ui.Control;
import totalcross.ui.Edit;
import totalcross.ui.Label;
import totalcross.ui.ListBox;
import totalcross.ui.MainWindow;
import totalcross.ui.PopupMenu;
import totalcross.ui.PushButtonGroup;
import totalcross.ui.Spacer;
import totalcross.ui.Toast;
import totalcross.ui.TopMenu;
import totalcross.ui.dialog.MessageBox;
import totalcross.ui.event.ControlEvent;
import totalcross.ui.event.Event;
import totalcross.ui.font.Font;
import totalcross.ui.gfx.Color;
import totalcross.ui.image.Image;
import totalcross.ui.image.ImageException;

public class CadastroExercicio extends MainWindow {
	private ListBox lb;
	private ComboBox combo;
	private Edit edObjetivo;
	private Edit edEquipamento;
	private Edit edSeries;
	private Edit edRepeticoes;
	
	private Image btnMenu;
	private Bar bar;
	private Button btnInserir;
	private Button btnLimpar; 
	private Button btnListar;
	private ExercicioDao exercicioDao;
	private Container containerMenu = new Container();
	private MessageBox message;
	PopupMenu pm ;
	public CadastroExercicio() throws ImageException, IOException {
//		super("Cadastrar Exercicios do Treino", HORIZONTAL_GRADIENT);
		setUIStyle(Settings.Android);
//		Container c = new Container();
//		c.add(new Label("eGym "));
//		Button btn = new Button("ic_action_back.png");
		bar = new Bar(" Cadastrar Exerc�cio");
		bar.setForeColor(Color.getRGB(236, 240, 241));
		btnMenu = new Image("/img/ic_action_sort_by_size.png");
		bar.addButton(btnMenu);
		bar.setBackColor(Color.getRGB(46, 204, 113));
//		setMenuBar(bar);
//		add(menubar, CENTER,TOP);
		add(bar,LEFT,0,FILL,PREFERRED);
	 	Settings.uiAdjustmentsBasedOnFontHeight = true;
		gradientTitleStartColor = Color.getRGB(22, 160, 133);
		gradientTitleEndColor = Color.getRGB(46, 204, 113);
		backColor = Color.getRGB(236, 240, 241);
		
		containerMenu.setBackColor(Color.getRGB(236, 240, 241));
//		containerMenu.setBackColor(Color.getRGB(236, 240, 241));
		add(containerMenu,RIGHT, AFTER, 150,PARENTSIZEMIN+15);
		btnListar = new Button("Exerc�cios");
		btnListar.setBorder(BORDER_NONE);
		btnListar.setRect(getAbsoluteRect());
		containerMenu.add(btnListar,CENTER, CENTER);
		containerMenu.backgroundStyle = BACKGROUND_CYLINDRIC_SHADED;
		containerMenu.setForeColor(Color.getRGB(236, 240, 241));
		containerMenu.setVisible(false);
		containerMenu.setBorderStyle(BORDER_ROUNDED);
		containerMenu.borderColor = Color.getRGB(189, 195, 199);
		
		message = new MessageBox("Confirmar exclus�o?", "Deseja excluir este registro?",new String[] {"Sim","N�o"});
//		message.popup();
		String[] items =
			   {
			         "Always",
			         "Never",
			         "Only in Silent mode",
			         "Only when not in Silent mode",
			         "Non of the answers above",
			   };
			   pm = new PopupMenu("Vibrate",items);
			  

		

	}
	
	// Initialize the user interface
	@Override
	public void initUI() {
		super.initUI();
		exercicioDao = new ExercicioDao();
		add(new Label("Objetivo"),LEFT,AFTER+50);
		add(edObjetivo = new Edit(),LEFT,AFTER);
		
		add(new Label("Equipamento"),LEFT,AFTER+50);
		add(edEquipamento = new Edit(),LEFT,AFTER);
		
		add(new Label("S�ries"),LEFT,AFTER+50);
		add(edSeries = new Edit(),LEFT,AFTER);
		
		add(new Label("Repeti��es"),LEFT,AFTER+50);
		add(edRepeticoes = new Edit(),LEFT,AFTER);
		
		Spacer sp = new Spacer(0,0);
		add(sp, CENTER,BOTTOM-300, PARENTSIZE+10, PREFERRED);
		
		add(btnInserir = new Button("Salvar"), BEFORE, SAME, PARENTSIZE+20, PREFERRED,sp);
		add(btnLimpar  = new Button("Limpar"),  AFTER,  SAME, PARENTSIZE+20, PREFERRED,sp);
		btnInserir.setForeColor(Color.WHITE);
		btnInserir.setBackColor(Color.getRGB(46, 204, 113));
		btnLimpar.setBackColor(Color.getRGB(236, 240, 241));
		if (Settings.onJavaSE || Settings.platform.equals(Settings.WIN32)){
			try {
//				apenas para o caso onde banco SQLite: 'test.db', não existe.
				exercicioDao.executarQuery("create table if not exists exercicio (objetivo varchar, equipamento varchar, series varchar, repeticoes varchar)");
	
			} catch (Exception e) {
	
				MessageBox.showException(e, true);
				exit(0);
	
			}
	
		}
		Control[] items = {new Button("Teste")};
		TopMenu top = new TopMenu(items, CENTER);
		setMenuBar(top);
	}
	
	private void salvar() throws SQLException {
		if (edEquipamento.getLength()==0 || edObjetivo.getLength()==0
				|| edRepeticoes.getLength() == 0 || edSeries.getLength() == 0) {
			Toast.foreColor=Color.WHITE;
			Toast.backColor=Color.getRGB(243, 156, 18);
			Toast.show("Preencha todos os campos corretamente.",3000);
		} else {
			Exercicio exercicio = new Exercicio();
			exercicio.setObjetivo(edObjetivo.getText());
			exercicio.setEquipamento(edEquipamento.getText());
			exercicio.setSeries(edSeries.getText());
			exercicio.setNumRepeticoes(edRepeticoes.getText());
			
			exercicioDao.inserir(exercicio);
			clear();
			
			Toast.foreColor=Color.WHITE;
			Toast.backColor=Color.getRGB(39, 174, 96);
			Toast.show("Dados inseridos com sucesso.", 3000);
			
		}
	}
	
	public Bar getBar() {
		return bar;
	}

	public void setBar(Bar bar) {
		this.bar = bar;
	}

	@Override
	public void onEvent(Event event) {
		// TODO Auto-generated method stub
		super.onEvent(event);
		switch (event.type) {
		case ControlEvent.PRESSED:
			if (event.target==btnInserir) {
				try {
					salvar();
				} catch (SQLException e) {
					Toast.foreColor=Color.WHITE;
					Toast.backColor=Color.getRGB(253, 57, 57);
					Toast.show("Erro inesperado, contate o administrador.", 4000);
					break;
				}
			} else if (event.target==btnLimpar) {
				clear();
			} else if (event.target==bar) {			
				containerMenu.setVisible(visualizacaoMenu());
			} else if (event.target==btnListar) {			
				CadastroExercicio.getMainWindow().swap(new ListarTreino());
			}
			break;
		default:
			break;
		}
	}

	private boolean visualizacaoMenu() {
		if (containerMenu.isVisible()==true) {
			return false;
		}else{
			return true;
		}
	}
	
	public Edit getEdObjetivo() {
		return edObjetivo;
	}

	public Edit getEdEquipamento() {
		return edEquipamento;
	}

	public Edit getEdSeries() {
		return edSeries;
	}

	public Edit getEdRepeticoes() {
		return edRepeticoes;
	}

	public void setEdObjetivo(Edit edObjetivo) {
		this.edObjetivo = edObjetivo;
	}

	public void setEdEquipamento(Edit edEquipamento) {
		this.edEquipamento = edEquipamento;
	}

	public void setEdSeries(Edit edSeries) {
		this.edSeries = edSeries;
	}

	public void setEdRepeticoes(Edit edRepeticoes) {
		this.edRepeticoes = edRepeticoes;
	}

	public ComboBox getCombo() {
		return combo;
	}

	public void setCombo(ComboBox combo) {
		this.combo = combo;
	}

	public ListBox getLb() {
		return lb;
	}

	public void setLb(ListBox lb) {
		this.lb = lb;
	}

	public Button getBtnInserir() {
		return btnInserir;
	}

	public Button getBtnLimpar() {
		return btnLimpar;
	}

	public void setBtnInserir(Button btnInserir) {
		this.btnInserir = btnInserir;
	}

	public void setBtnLimpar(Button btnLimpar) {
		this.btnLimpar = btnLimpar;
	}

	public ExercicioDao getExercicioDao() {
		return exercicioDao;
	}

	public void setExercicioDao(ExercicioDao exercicioDao) {
		this.exercicioDao = exercicioDao;
	}
	
	public Container getContainerMenu() {
		return containerMenu;
	}

	public void setContainerMenu(Container containerMenu) {
		this.containerMenu = containerMenu;
	}

	public Image getBtnMenu() {
		return btnMenu;
	}

	public void setBtnMenu(Image btnMenu) {
		this.btnMenu = btnMenu;
	}

	public Button getBtnListar() {
		return btnListar;
	}

	public void setBtnListar(Button btnListar) {
		this.btnListar = btnListar;
	}
}
